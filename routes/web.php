<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\YoloController;
//use App\Http\Controllers\PingPongController;
use App\Http\Controllers\DemoControlleur;
use App\Http\Controllers\PingPongControlleur;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/ping',[PingPongControlleur::class,'ping']);
Route::get('/ping/list',[DemoControlleur::class,'list']);
Route::get('/ping/form',[DemoControlleur::class,'store'])->name('store_form');

Route::get('/yolo',[YoloController::class,'View']);
Route::get('/yolo-connected',[YoloController::class,'privateView'])->middleware(['auth'])->name("yoloPrivate");;


require __DIR__.'/auth.php';
