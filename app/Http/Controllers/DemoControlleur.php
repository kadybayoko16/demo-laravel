<?php

namespace App\Http\Controllers;

use App\Models\Ping;
use Illuminate\Http\Request;

class DemoControlleur extends Controller
{
    //
     //Affichons tout les entrées

     public function list()
     {
         //$liste= Ping::where('texte', "YOLO")->orderBy('id')->take(10)
         $liste = Ping::all();
         return view('todoList.demo',compact('liste'));
     }

     //Creer des formulaires Post

     public function store(Request $request){
        //Ping::create($request->all());
         //return redirect("/ping");
         return view('todoList.form_add');
     }
}
