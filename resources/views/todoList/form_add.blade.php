@extends('layouts.base')

@section('title',"liste des acteurs")
@section('content')

<div class="card">
    <div class="card-body">

        <form class="row g-5 needs-validation" action="{{ route('store_form') }}">
            <div class="col-md-12">
              <label for="validationCustom01" class="form-label">Enter the text</label>
              <textarea class="form-control is-invalid" id="validationTextarea" placeholder="Required example textarea" required></textarea>
              <div class="valid-feedback">
                Looks good!
              </div>
            </div>
            <div class="col-12">
              <button class="btn btn-primary" type="submit">Submit form</button>
            </div>
          </form>

    </div>
  </div>



@endsection
