@extends('layouts.base')

@section('title',"liste des acteurs")
@section('content')

<table class="table w-10">
    <thead>
      <tr>

        <th scope="col">id</th>
        <th scope="col">Text</th>
      </tr>
    </thead>
    <tbody>
        @foreach($liste as $list)
      <tr>
        <th scope="row">{{$list->id}}</th>
        <td>{{$list->texte}}</td>

      </tr>
      @endforeach
    </tbody>
  </table>

@endsection
